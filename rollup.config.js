import replace from "@rollup/plugin-replace";
import { terser } from "rollup-plugin-terser";
import { nodeResolve } from "@rollup/plugin-node-resolve";

function config({ input, format, minify }) {
  const minifierSuffix = minify ? ".min" : "";
  return {
    input: `./src/${input}.js`,
    output: {
      name: input,
      file: `dist/${format}/${input}${minifierSuffix}.js`,
      format,
      sourcemap: true,
    },
    plugins: [
      // replace({
      //   "process.env.NODE_ENV": minify ? "'production'" : "'developement'",
      //   preventAssignment: true,
      // }),
      nodeResolve(),
      minify
        ? terser({
            compress: true,
            mangle: true,
          })
        : undefined,
    ].filter(Boolean),
  };
}

export default [
  { input: "index", format: "esm", minify: false },
  { input: "index", format: "esm", minify: true },
  { input: "worker", format: "esm", minify: false },
  { input: "worker", format: "esm", minify: true },
  { input: "index", format: "umd", minify: false },
  { input: "index", format: "umd", minify: true },
  { input: "worker", format: "umd", minify: false },
  { input: "worker", format: "umd", minify: true },
].map(config);
