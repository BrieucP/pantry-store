const getPath = (obj, path) => {
  let i,
    len = path.length;
  for (i = 0; i < len; i++) {
    obj = obj ? obj[path[i]] : undefined;
  }
  return obj;
};

const forEach = (arr, callback) => {
  let i,
    len = arr.length;
  for (i = 0; i < len; i++) {
    callback(arr[i], i, arr);
  }
};

export const applyPatches = (o) => (patches) => {
  forEach(patches, ({ value, path, op, prop }) => {
    let cursor = getPath(o, path);
    let finalValue = { [prop]: value };
    let parentPath = path;
    switch (op) {
      case "add":
        while (!cursor) {
          finalValue = { [parentPath[parentPath.length - 1]]: finalValue };
          parentPath = parentPath.slice(0, parentPath.length - 1);
          cursor = getPath(o, parentPath);
        }
        Object.assign(cursor, finalValue);
        break;
      case "update":
        if (cursor) cursor[prop] = value;
        break;
      case "delete":
        if (cursor) delete cursor[prop];
        break;
      default:
        throw Error(`Operation: ${op} is not valid`);
    }
  });
  return o;
};

export const generatePatches = (o) => {
  const patches = [];
  let path = [];
  const handler = {
    deleteProperty(target, prop) {
      if (target === o) path = [];
      if (prop in target) {
        patches.push({
          op: "delete",
          path,
          prop,
        });
        return Reflect.deleteProperty(target, prop);
      }
    },
    set(target, prop, value) {
      if (target === o) path = [];
      patches.push({
        op: target[prop] !== undefined ? "update" : "add",
        path,
        value,
        prop,
      });
      return Reflect.set(target, prop, value);
    },
    get(target, prop, receiver) {
      if (target === o) path = [];
      if (typeof target[prop] === "object") {
        path.push(prop);
        return new Proxy(target[prop], handler);
      }
      return Reflect.get(target, prop, receiver);
    },
  };
  const proxy = new Proxy(o, handler);
  return (f) =>
    async (...payload) => {
      if (f) {
        await f(proxy, ...payload);
      }
      return patches;
    };
};
