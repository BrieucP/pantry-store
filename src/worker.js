import { DISPATCH, PUBLISH } from "./utils/const";
// import { generatePatches } from ".//utils/patch";

import { produce, enablePatches } from "immer";

enablePatches();

const generatePatches =
  (o) =>
  (f) =>
  async (...payload) => {
    let patches = [];
    o = await produce(
      o,
      (draft) => f(draft, ...payload),
      (patchList) => (patches = patchList)
    );
    return patches;
  };

export const createRemoteStore = (initialState = {}) => {
  const commitsRecord = Object.assign({});
  const actionsRecord = Object.assign({});
  const proxyStore = generatePatches(initialState);
  const compute = async (action, ...payload) => {
    if (commitsRecord[action]) {
      // COMMITS
      return await proxyStore(commitsRecord[action])(...payload);
    } else if (actionsRecord[action]) {
      // ACTIONS
      const readoOnlyState = initialState;
      await actionsRecord[action](
        { state: readoOnlyState, dispatch },
        ...payload
      );
      return Promise.resolve([]);
    } else {
      throw Error(`No actions: ${action} has been registered`);
    }
  };
  const publish = (patches, id = null) => {
    self.postMessage({ type: PUBLISH, patches, id });
  };
  const dispatch = async (action, ...payload) => {
    publish(await compute(action, ...payload));
  };
  self.addEventListener("message", async function onDispatch({ data }) {
    if (data.type === DISPATCH) {
      publish(await compute(data.action, ...data.payload), data.id);
    }
  });
  return {
    dispatch,
    registerDispatcher: (commits, actions) => {
      Object.assign(commitsRecord, commits);
      Object.assign(actionsRecord, actions);
    },
  };
};
