import { DISPATCH, PUBLISH } from "./utils/const";
import { applyPatches, enablePatches } from "immer";

enablePatches();

function Deferred() {
  this.promise = new Promise((resolve, reject) => {
    this.reject = reject;
    this.resolve = resolve;
  });
}

export const connectToRemoteStore = (remoteStoreWorker, initialState = {}) => {
  let state = initialState;
  const subscribers = new Set();
  const dispatched = new Map();
  let uid = Math.random();
  remoteStoreWorker.addEventListener("message", function onPublish({ data }) {
    if (data.type === PUBLISH) {
      if (Array.isArray(data.patches) && data.patches.length) {
        state = applyPatches(state, data.patches);
        subscribers.forEach((f) => f(state));
      }
      if (data.id) {
        const dispatchPromise = dispatched.get(data.id);
        if (dispatchPromise) {
          dispatchPromise.resolve(state);
          dispatched.delete(data.id);
        }
      }
    }
  });
  return {
    dispatch: async (action, ...payload) => {
      const id = ++uid;
      dispatched.set(id, new Deferred());
      remoteStoreWorker.postMessage({ type: DISPATCH, id, action, payload });
      return await dispatched.get(id).promise;
    },
    get state() {
      return state;
    },
    subscribe: (listener) => {
      subscribers.add(listener);
      return () => subscribers.delete(listener);
    },
  };
};
