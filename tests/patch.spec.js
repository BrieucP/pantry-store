import { generatePatches, applyPatches } from "../utils/patch";
import test from "ava";

test("generatePatches should create a list of patches", async function (t) {
  const target = {
    count: 0,
  };
  const source = {
    count: 0,
  };
  const expectedPatches = [
    {
      op: "update",
      path: [],
      value: 1,
      prop: "count",
    },
    {
      op: "add",
      path: [],
      value: {
        another: {
          test: 2,
        },
      },
      prop: "deep",
    },
    {
      op: "add",
      path: ["deep"],
      value: {},
      prop: "obj",
    },
    {
      op: "add",
      path: ["deep", "obj"],
      value: 0,
      prop: "a",
    },
    {
      op: "delete",
      path: ["deep", "obj"],
      prop: "a",
    },
    {
      op: "add",
      path: [],
      value: [0, 21, 7, 1, 2],
      prop: "demo",
    },
    {
      op: "add",
      path: ["deep"],
      value: {
        test: 2,
      },
      prop: "another",
    },
    {
      op: "add",
      path: ["deep", "another"],
      value: 1,
      prop: "test",
    },
    {
      op: "add",
      path: [],
      value: {
        else: {
          entirely: 2,
        },
      },
      prop: "something",
    },
    {
      op: "update",
      path: ["something", "else"],
      value: 2,
      prop: "entirely",
    },
    {
      op: "update",
      path: ["deep", "another"],
      value: 2,
      prop: "test",
    },
    {
      op: "delete",
      path: ["deep"],
      prop: "obj",
    },
    {
      op: "update",
      path: ["demo"],
      value: 7,
      prop: "2",
    },
    {
      op: "add",
      path: ["demo"],
      value: 1,
      prop: "3",
    },
    {
      op: "add",
      path: ["demo"],
      value: 2,
      prop: "4",
    },
    {
      op: "add",
      path: ["demo"],
      value: 3,
      prop: "5",
    },
    {
      op: "update",
      path: ["demo"],
      value: 6,
      prop: "length",
    },
    {
      op: "delete",
      path: ["demo"],
      prop: "5",
    },
    {
      op: "update",
      path: ["demo"],
      value: 5,
      prop: "length",
    },
    {
      op: "add",
      path: [],
      value: 2,
      prop: "testAsync",
    },
    {
      op: "add",
      path: [],
      value: {
        new: true,
      },
      prop: "new",
    },
    {
      op: "add",
      path: ["new"],
      value: true,
      prop: "new",
    },
    {
      op: "add",
      path: [],
      value: true,
      prop: "second",
    },
  ];

  const patches = await generatePatches(source)(async (o) => {
    o.count++;
    o.deep = {};
    o.deep.obj = {};
    o.deep.obj.a = 0;
    delete o.deep.obj.a;
    o.demo = [0, 21, 225];
    o.deep.another = {};
    o.deep.another.test = 1;
    o.something = { else: { entirely: 1 } };
    o.something.else.entirely++;
    o.deep.another.test++;
    delete o.deep.obj;
    o.demo[2] = 7;
    o.demo.push(1, 2, 3);
    o.demo.pop();
    o.testAsync = await new Promise((res) => setTimeout(() => res(2), 200));
    o.new = {};
    o.new.new = !o.new.new;
    o.second = !o.second;
  })();
  t.deepEqual(expectedPatches, patches);

  applyPatches(target)(patches);

  t.deepEqual(source, target);
});
